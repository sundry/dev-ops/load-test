const { app, start } = require("boilerpress");
const os = require("os");
app.get("/hello", (req, res) => {
  res.send("Hello World!");
});
app.get("/loop", (req, res) => {
  var c = 0;
  for (var i = 0; i < 1000; i++) {
    for (var j = 0; j < 10000000; j++) {
      c++;
    }
  }
  res.send(c.toString());
});
// Start the Express server
start();
