#!/bin/bash

# Run load tests
artillery run node.yml -o ./reports/node.json
artillery run go.yml -o ./reports/go.json
artillery run python.yml -o ./reports/python.json

# Convert output to html charts
artillery report ./reports/node.json -o ./reports/html/node.html
artillery report ./reports/go.json -o ./reports/html/go.html
artillery report ./reports/python.json -o ./reports/html/python.html
