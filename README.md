# load-test

## Running the Docker services

Build and start all of the services

```
docker-compose up
```

Anytime you change a file you will need to rebuild the services

```
docker-compose build
```

## Run the artillery load test

```
cd ./artillery

./start.sh
```

Or you can run each load test individually

```
artillery run node.yml -o ./reports/node.json
```

If you want to convert the json output to a readable html graph

```
artillery report ./reports/node.json -o ./reports/html/node.html
```
