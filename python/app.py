import os
from flask import Flask

port = int(os.environ.get('PORT', 5000))
app = Flask(__name__)

@app.route('/')
@app.route('/hello')
def index():
    return "Hello World!"

# https://stackoverflow.com/questions/8097408/why-python-is-so-slow-for-a-simple-for-loop
@app.route('/loop')
def loop():
    c = 0
    for i in range(1000):
        for j in range(10000000):
            c += 1
    return c

if __name__ == "__main__":
    app.run(debug = True, host = '0.0.0.0', port=port)
